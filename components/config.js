import YAML from 'yaml'
import fs from 'fs'
import path from 'path'
const _path = process.cwd().replace(/\\/g, '/')
// 插件名
const pluginName = path.basename(path.join(import.meta.url, '../../'))
class Config {
    constructor() {
        this.pluginRoot = path.join(_path, 'plugins', pluginName)
        this.initConfig()
    }

    getConfig() {
        try {
            const config_data = YAML.parse(
                fs.readFileSync(`${this.pluginRoot}/config/config/config.yaml`, 'utf-8')
            );

            // 默认值定义
            const defaultGroupConfig = {
                nsfw: false,
                meme: false,
                bangumi: false,
            };

            // 填充默认值
            if (Array.isArray(config_data.GroupList)) {
                config_data.GroupList = config_data.GroupList.map(group => ({
                    ...defaultGroupConfig, // 填充默认值
                    ...group,             // 自定义配置覆盖默认值
                }));
            } else {
                config_data.GroupList = [];
            }

            config_data.enable = config_data.enable ?? true;

            return config_data;
        } catch (err) {
            logger.error('【TG-Plugin】读取config.yaml失败', err)
            return false
        }
    }


    setConfig(config_data) {
        try {
            fs.writeFileSync(
                `${this.pluginRoot}/config/config/config.yaml`,
                YAML.stringify(config_data),
            )
            return true
        } catch (err) {
            logger.error('【TG-Plugin】写入config.yaml失败', err)
            return false
        }
    }

    getDefConfig() {
        try {
            const config_default_data = YAML.parse(
                fs.readFileSync(`${this.pluginRoot}/config/default_config/config.yaml`, 'utf-8')
            )
            return config_default_data
        } catch (err) {
            logger.error('【TG-Plugin】读取default_config/config.yaml失败', err)
            return false
        }
    }

    initConfig() {
        const config_default_path = `${this.pluginRoot}/config/default_config/config.yaml`
        if (!fs.existsSync(config_default_path)) {
            logger.error('【TG-Plugin】默认设置文件不存在，请检查或重新安装插件')
            return true
        }
        const config_path = `${this.pluginRoot}/config/config/config.yaml`
        if (!fs.existsSync(config_path)) {
            logger.error('【TG-Plugin】设置文件不存在，将使用默认设置文件')
            fs.copyFileSync(config_default_path, config_path)
        }
        const config_default_yaml = this.getDefConfig()
        const config_yaml = this.getConfig()
        for (const key in config_default_yaml) {
            if (!(key in config_yaml)) {
                config_yaml[key] = config_default_yaml[key]
            }
        }
        for (const key in config_yaml) {
            if (!(key in config_default_yaml)) {
                delete config_yaml[key]
            }
        }
        this.setConfig(config_yaml)
    }
}

export default new Config()
