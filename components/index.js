import { getRandom, sleep, HourlyRateLimiter } from '../utils/utils.js';
import Config from "./config.js";
function initWebSocket() {
    const rateLimiter = new HourlyRateLimiter();
    Bot.on('message.group.normal', async e => {
        let config = Config.getConfig();
        let groupList = config.GroupList;
        if (groupList && groupList.length && config.enable && e.group_id == 763615509) {
            let msg = e.message[e.message.length - 1];
            let title = msg.type == 'text' ? msg.text : ''
            for (let index = 0; index < groupList.length; index++) {
                let v = groupList[index];
                if (title.includes('NSFW') && !v.nsfw) return false;
                if (title.startsWith('https://m.q.qq.com') && !v.bangumi) return false;
                if (!title.startsWith('https://m.q.qq.com') && !v.meme ) return false;
                if (v.limit && !rateLimiter.canTrigger(v.group_id, v.limit)) return false;
                try {
                    await Bot.pickGroup(parseInt(v.group_id)).sendMsg(e.message.length > 4 ? Bot.makeForwardMsg({ message: e.message, nickname: Bot.nickname, user_id: Bot.uin }) : e.message)
                } catch (error) {
                    await e.bot.pickGroup(parseInt(v.group_id)).sendMsg(e.message.length > 4 ? e.group.makeForwardMsg({ message: e.message, nickname: e.bot.nickname, user_id: e.self_id }) : e.message)
                }
                await sleep(getRandom(3000, 5000))
            }
        }
    })
}

export {
    initWebSocket,
}

