import plugin from '../../../lib/plugins/plugin.js'
import Config from "../components/config.js";
export class setting extends plugin {
    constructor() {
        super({
            name: '[tg-plugin] 设置',
            dsc: '[tg-plugin] 设置',
            event: 'message',
            priority: 100,
            rule: [
                {
                    reg: '^#tg(打开|关闭|查看)连接.*$',
                    fnc: 'modifyWs',
                    permission: 'master'
                },
                {
                    reg: '^#tg帮助$',
                    fnc: 'help'
                },
            ]
        })
    }

    async modifyWs(e) {
        let reg = new RegExp('^#tg(打开|关闭|查看)连接(.*)$')
        let config = Config.getConfig();
        let regRet = reg.exec(e.msg)
        if (!regRet) return false;
        switch (regRet[1]) {
            case '打开':
                config.enable = true;
                Config.setConfig(config);
                e.reply('操作成功~')
                break
            case '关闭':
                config.enable = false;
                Config.setConfig(config);
                e.reply('操作成功~')
                break
            case '查看':
                e.reply(`当前状态:${config.enable ? '已连接' : '未连接'}`)
                break
        }
    }

    async help(e) {
        await e.reply([
            '欢迎使用沙雕插件【tg-plugin】\n',
            '【#tg[打开|关闭|查看]连接】设置连接状态\n',
            '群策略仅支持在锅巴插件中配置',
        ])
        return true
    }
}