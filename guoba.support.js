import Config from "./components/config.js";
import lodash from "lodash";

export function supportGuoba() {
    return {
        configInfo: {
            schemas: [
                {
                    field: "GroupList",
                    label: "群策略",
                    bottomHelpMessage: "设置需要接收消息推送的群号",
                    component: "GSubForm",
                    componentProps: {
                        multiple: true,
                        schemas: [
                            {
                                field: "group_id",
                                label: "群号",
                                component: "Input",
                                bottomHelpMessage: "设置需要接收消息推送的群号",
                                required: true,
                                componentProps: {
                                    placeholder: "请输入群号",
                                },
                            },
                            {
                                field: "limit",
                                label: "频率",
                                component: "InputNumber",
                                bottomHelpMessage: "每小时最多发送几条消息，不填表示不限制",
                                componentProps: {
                                    placeholder: "请输入数字",
                                    addonAfter: "次",
                                },
                            },
                            {
                                field: "nsfw",
                                label: "NSFW",
                                component: "Switch",
                                bottomHelpMessage: "是否推送不适宜在公共场合查看的消息（不包括露点图）",
                            },
                            {
                                field: "bangumi",
                                label: "新番",
                                component: "Switch",
                                bottomHelpMessage: "是否推送每日动漫新番消息",
                            },
                            {
                                field: "meme",
                                label: "梗图",
                                component: "Switch",
                                bottomHelpMessage: "是否推送梗图消息",
                            },
                        ],
                    },
                },
                {
                    field: "enable",
                    label: "连接状态",
                    bottomHelpMessage: "默认开启，关闭后将不会继续推送消息",
                    component: "Switch",
                }
            ],
            getConfigData() {
                return Config.getConfig();
            },
            setConfigData(data, { Result }) {
                let config = {}
                for (let [keyPath, value] of Object.entries(data)) {
                    lodash.set(config, keyPath, value)
                }
                config = lodash.merge({}, Config.getConfig(), config)
                config.GroupList = data['GroupList']
                Config.setConfig(config)
                return Result.ok({}, '保存成功~')
            },
        },
    };
}
