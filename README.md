# tg-plugin

## 介绍
Yunzai-Bot 的扩展插件 tg-plugin 提供沙雕图和新番消息的自动推送

## 安装教程

在云崽根目录输入以下命令

```
git clone --depth=1 https://gitee.com/misakime/tg-plugin.git ./plugins/tg-plugin/
```
## 使用说明

使用前请先关闭自动退群功能，再用机器人号加入Q群：***763615509*** ，进群答案：是

安装插件后请先在锅巴中配置群策略

本插件除了管理类的指令外没有其他用户交互类指令

<details>
<summary>功能列表 | 只支持主人使用</summary>

| 指令          | 说明                         |
| ------------  | --------------------------- |
| #tg帮助       | 沙雕插件的使用说明            |
| #tg更新       | 更新插件            |
| #tg关闭连接    | 关闭消息自动推送             |
| #tg打开连接    | 打开消息自动推送             |
| #tg查看连接    | 查看当连接状态               |

</details>

## 交流群

QQ群：866938609

## 免责声明

1. 功能仅限内部交流与小范围使用，请勿将Yunzai-Bot及tg-plugin用于以盈利为目的的场景
2. 图片与其他素材均来自于网络，仅供娱乐测试使用，如有侵权请联系，会立即删除
3. 使用此插件产生的一切后果与本人均无关

## 其他

如果觉得此插件对你有帮助的话,可以点一个star,你的支持就是不断更新的动力~

## 访问量

[![访问量](https://profile-counter.glitch.me/misakime-tg-plugin.git/count.svg)](https://gitee.com/misakime/tg-plugin)